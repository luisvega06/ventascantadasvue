<?php

namespace App\Http\Controllers;
use DB;
use App\Models\PointSale;
use Illuminate\Http\Request;

class PointSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$point_sale = PointSale::all();
        $regionals =  DB::select("SELECT  point_sales.regional  FROM point_sales GROUP BY point_sales.regional");
        return response()->json(["punto_de_venta"=>$regionals]);
    }

    public function searchDepartment($regional)
    {
        $departments = DB::select("SELECT point_sales.department FROM point_sales WHERE point_sales.regional = '".$regional ."' GROUP BY point_sales.department");
        return response()->json(["departments"=>$departments]);
    }

    public function searchChannel($department)
    {
        $channels = DB::select("SELECT point_sales.channel FROM point_sales WHERE point_sales.department = '".$department ."' GROUP BY point_sales.channel");
        return response()->json(["channels"=>$channels]);
    }

    public function searchStore($channel, $department)
    {
       /* $stores = DB::select("SELECT point_sales.name, point_sales.id 
        FROM point_sales 
        WHERE point_sales.channel = '".$channel ."' AND ");*/
        $stores = DB::table('point_sales')
                ->select('point_sales.name','point_sales.id', 'point_sales.channel', 'point_sales.department')
                ->where([
                    ['channel',$channel],
                    ['department',$department]
                ])
            ->get();
        return response()->json(["stores"=>$stores]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pont_sale = PointSale::create($request->input());
        return response()->json(["punto_de_venta"=>$pont_sale]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PointSale  $pointSale
     * @return \Illuminate\Http\Response
     */
    public function show(PointSale $pointSale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PointSale  $pointSale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PointSale $pointSale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PointSale  $pointSale
     * @return \Illuminate\Http\Response
     */
    public function destroy(PointSale $pointSale)
    {
        //
    }
}
