<?php

namespace App\Http\Controllers;
use App\Models\PointSale;
use App\Models\Sale;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$sale = Sale::with(['point_sales'])->get();
        $sale = DB::table('sales')
                    ->join('point_sales','sales.point_sale_id','point_sales.id')
                    ->select(
                        'point_sales.regional',
                        DB::raw('SUM(sales.new) as linea_nueva'),
                        DB::raw('SUM(sales.migration) as migration'),
                        DB::raw('SUM(sales.cover_page) as portabilidad')
                    )
                    
                   ->GROUPBY('point_sales.regional') 
        ->get();
        return response()->json(["sale"=>$sale]);

    }

    public function reportPoint($department, $cut, $option)
    {
        if($option == 1){
            $starDate = Carbon::now()->format('Y-m-d');
            $endDate = Carbon::now()->copy()->endOfDay();
        }else{
            $starDate = Carbon::now()->subDays(1)->format('Y-m-d');
            $endDate = Carbon::now()->subDays(1)->copy()->endOfDay();
        }
        $pointDetalle = DB::table('sales')
            ->join('point_sales','sales.point_sale_id','point_sales.id')
            ->select(
                'point_sales.department',
                'point_sales.name',
                'sales.cut',
                DB::raw('SUM(sales.new) as linea_nueva'),
                DB::raw('SUM(sales.migration) as migration'),
                DB::raw('SUM(sales.cover_page) as portabilidad'),
                DB::raw("SUM(sales.new + sales.migration + sales.cover_page) AS 'TOTAL'"),
                DB::raw('SUM(sales.rgu) as home'),
                DB::raw('SUM(sales.turno_r) as turnoR'),
                DB::raw('SUM(sales.turno_c) as turnoC'),
                DB::raw('SUM(sales.turno_total) as turnos_total'),
                DB::raw('SUM(sales.goal_pos) as meta_pos'),
                DB::raw('SUM(sales.goal_rgu) as meta_home'),
                DB::raw('SUM(sales.goal_new) as meta_linea_nueva'),
                DB::raw('SUM(sales.goal_migration) as meta_migration'),
                DB::raw('SUM(sales.goal_porta) as meta_porta')
            )
            ->whereBetween('sales.created_at', [$starDate, $endDate])
            ->where([
                ['point_sales.department',$department],
                ['sales.cut', $cut ]
            ])
            ->GROUPBY('point_sales.name') 
             

        ->get();
        return response()->json(["points"=>$pointDetalle]);

    }

    public function getGoal($id){
        $starDate = Carbon::now()->format('Y-m-d');
        $endDate = Carbon::now()->copy()->endOfDay();
        $goal = DB::table('sales')
            ->select(
                'sales.goal_pos',
                'sales.goal_rgu',
                'sales.goal_new',
                'sales.goal_migration',
                'sales.goal_porta',
                'sales.point_sale_id',
                'sales.cut'
            )
            ->whereBetween('sales.created_at', [$starDate, $endDate])
            ->where([
                ['sales.point_sale_id',$id]
            ])
        ->first();
        return response()->json(["metas"=>$goal]);
    }

    public function reportDepartment($regional, $cut, $option)
    {
        if($option == 1){
            $starDate = Carbon::now()->format('Y-m-d');
            $endDate = Carbon::now()->copy()->endOfDay();
        }else{
            $starDate = Carbon::now()->subDays(1)->format('Y-m-d');
            $endDate = Carbon::now()->subDays(1)->copy()->endOfDay();
        }
        
        $detalle = DB::table('sales')
                    ->join('point_sales','sales.point_sale_id','point_sales.id')
                    ->select(
                        'point_sales.department',
                        'sales.cut',
                        DB::raw('SUM(sales.new) as linea_nueva'),
                        DB::raw('SUM(sales.migration) as migration'),
                        DB::raw('SUM(sales.cover_page) as portabilidad'),
                        DB::raw("SUM(sales.new + sales.migration + sales.cover_page) AS 'TOTAL'"),
                        DB::raw('SUM(sales.rgu) as home'),
                        DB::raw('SUM(sales.turno_r) as turnoR'),
                        DB::raw('SUM(sales.turno_c) as turnoC'),
                        DB::raw('SUM(sales.turno_total) as turnos_total'),
                        DB::raw('SUM(sales.goal_pos) as meta_pos'),
                        DB::raw('SUM(sales.goal_rgu) as meta_home'),
                        DB::raw('SUM(sales.goal_new) as meta_linea_nueva'),
                        DB::raw('SUM(sales.goal_migration) as meta_migration'),
                        DB::raw('SUM(sales.goal_porta) as meta_porta')
                    )
                    ->whereBetween('sales.created_at', [$starDate, $endDate])
                    ->where([
                        ['sales.cut', $cut ],
                        ['point_sales.regional', $regional]
                    ])
                   ->GROUPBY('point_sales.department') 
        ->get();

        return response()->json(["detalle"=>$detalle]);

    }

    /*public function donwload($cut)
    {
        $starDate = Carbon::now()->format('Y-m-d');
        $endDate = Carbon::now()->copy()->endOfDay();
        $sale = DB::table('sales')
                    ->join('point_sales','sales.point_sale_id','point_sales.id')
                    ->select(
                        'point_sales.regional',
                        'sales.cut',
                        DB::raw('SUM(sales.new) as linea_nueva'),
                        DB::raw('SUM(sales.migration) as migration'),
                        DB::raw('SUM(sales.cover_page) as portabilidad'),
                        DB::raw("sales.new + sales.migration + sales.cover_page AS 'TOTAL'")
                        
                    )
                    ->whereBetween('sales.created_at', [$starDate, $endDate])
                    ->where('sales.cut','=',$cut)
                   ->GROUPBY('point_sales.regional') 
        ->get();
        return response()->json(["sale"=>$sale]);
    }*/

    public function reports($cut, $option)
    {
        if($option == 1){
            $starDate = Carbon::now()->format('Y-m-d');
            $endDate = Carbon::now()->copy()->endOfDay();
        }else{
            $starDate = Carbon::now()->subDays(1)->format('Y-m-d');
            $endDate = Carbon::now()->subDays(1)->copy()->endOfDay();
        }
        
        $sale = DB::table('sales')
                    ->join('point_sales','sales.point_sale_id','point_sales.id')
                    ->select(
                        'point_sales.regional',
                        'sales.cut',
                        DB::raw('SUM(sales.new) as linea_nueva'),
                        DB::raw('SUM(sales.migration) as migration'),
                        DB::raw('SUM(sales.cover_page) as portabilidad'),
                        DB::raw("SUM(sales.new + sales.migration + sales.cover_page) AS 'TOTAL'"),
                        DB::raw('SUM(sales.rgu) as home'),
                        DB::raw('SUM(sales.turno_r) as turnoR'),
                        DB::raw('SUM(sales.turno_c) as turnoC'),
                        DB::raw('SUM(sales.turno_total) as turnos_total'),
                        DB::raw('SUM(sales.goal_pos) as meta_pos'),
                        DB::raw('SUM(sales.goal_rgu) as meta_home'),
                        DB::raw('SUM(sales.goal_new) as meta_linea_nueva'),
                        DB::raw('SUM(sales.goal_migration) as meta_migration'),
                        DB::raw('SUM(sales.goal_porta) as meta_porta')
                        
                    )
                    ->whereBetween('sales.created_at', [$starDate, $endDate])
                    ->where('sales.cut','=',$cut)
                   ->GROUPBY('point_sales.regional') 
        ->get();
        return response()->json(["sale"=>$sale]);
    }

    public function reportsChannel($cut, $option)
    {
        if($option == 1){
            $starDate = Carbon::now()->format('Y-m-d');
            $endDate = Carbon::now()->copy()->endOfDay();
        }else{
            $starDate = Carbon::now()->subDays(1)->format('Y-m-d');
            $endDate = Carbon::now()->subDays(1)->copy()->endOfDay();
        }
        
        $sale_channel = DB::table('sales')
                    ->join('point_sales','sales.point_sale_id','point_sales.id')
                    ->select(
                        'point_sales.channel',
                        'sales.cut',
                        DB::raw('SUM(sales.new) as linea_nueva'),
                        DB::raw('SUM(sales.migration) as migration'),
                        DB::raw('SUM(sales.cover_page) as portabilidad'),
                        DB::raw("SUM(sales.new + sales.migration + sales.cover_page) AS 'TOTAL'"),
                        DB::raw('SUM(sales.rgu) as home'),
                        DB::raw('SUM(sales.turno_r) as turnoR'),
                        DB::raw('SUM(sales.turno_c) as turnoC'),
                        DB::raw('SUM(sales.turno_total) as turnos_total'),
                        DB::raw('SUM(sales.goal_pos) as meta_pos'),
                        DB::raw('SUM(sales.goal_rgu) as meta_home'),
                        DB::raw('SUM(sales.goal_new) as meta_linea_nueva'),
                        DB::raw('SUM(sales.goal_migration) as meta_migration'),
                        DB::raw('SUM(sales.goal_porta) as meta_porta')
                        
                    )
                    ->whereBetween('sales.created_at', [$starDate, $endDate])
                    ->where('sales.cut','=',$cut)
                   ->GROUPBY('point_sales.channel') 
        ->get();
        return response()->json(["sale_channel"=>$sale_channel]);
    }

    public function reporRegionalMew($channel, $cut, $option)
    {
        if($option == 1){
            $starDate = Carbon::now()->format('Y-m-d');
            $endDate = Carbon::now()->copy()->endOfDay();
        }else{
            $starDate = Carbon::now()->subDays(1)->format('Y-m-d');
            $endDate = Carbon::now()->subDays(1)->copy()->endOfDay();
        }
        
        $detalle = DB::table('sales')
                    ->join('point_sales','sales.point_sale_id','point_sales.id')
                    ->select(
                        'point_sales.channel',
                        'point_sales.regional',
                        'sales.cut',
                        DB::raw('SUM(sales.new) as linea_nueva'),
                        DB::raw('SUM(sales.migration) as migration'),
                        DB::raw('SUM(sales.cover_page) as portabilidad'),
                        DB::raw("SUM(sales.new + sales.migration + sales.cover_page) AS 'TOTAL'"),
                        DB::raw('SUM(sales.rgu) as home'),
                        DB::raw('SUM(sales.turno_r) as turnoR'),
                        DB::raw('SUM(sales.turno_c) as turnoC'),
                        DB::raw('SUM(sales.turno_total) as turnos_total'),
                        DB::raw('SUM(sales.goal_pos) as meta_pos'),
                        DB::raw('SUM(sales.goal_rgu) as meta_home'),
                        DB::raw('SUM(sales.goal_new) as meta_linea_nueva'),
                        DB::raw('SUM(sales.goal_migration) as meta_migration'),
                        DB::raw('SUM(sales.goal_porta) as meta_porta')
                    )
                    ->whereBetween('sales.created_at', [$starDate, $endDate])
                    ->where([
                        ['sales.cut', $cut ],
                        ['point_sales.channel', $channel]
                    ])
                   ->GROUPBY('point_sales.regional') 
        ->get();

        return response()->json(["detalle"=>$detalle]);

    }

    public function RegionalChannelDepartment($regional, $channel, $cut, $option)
    {
        if($option == 1){
            $starDate = Carbon::now()->format('Y-m-d');
            $endDate = Carbon::now()->copy()->endOfDay();
        }else{
            $starDate = Carbon::now()->subDays(1)->format('Y-m-d');
            $endDate = Carbon::now()->subDays(1)->copy()->endOfDay();
        }
        
        $detalle = DB::table('sales')
                    ->join('point_sales','sales.point_sale_id','point_sales.id')
                    ->select(
                        'point_sales.channel',
                        'point_sales.regional',
                        'point_sales.department',
                        'sales.cut',
                        DB::raw('SUM(sales.new) as linea_nueva'),
                        DB::raw('SUM(sales.migration) as migration'),
                        DB::raw('SUM(sales.cover_page) as portabilidad'),
                        DB::raw("SUM(sales.new + sales.migration + sales.cover_page) AS 'TOTAL'"),
                        DB::raw('SUM(sales.rgu) as home'),
                        DB::raw('SUM(sales.turno_r) as turnoR'),
                        DB::raw('SUM(sales.turno_c) as turnoC'),
                        DB::raw('SUM(sales.turno_total) as turnos_total'),
                        DB::raw('SUM(sales.goal_pos) as meta_pos'),
                        DB::raw('SUM(sales.goal_rgu) as meta_home'),
                        DB::raw('SUM(sales.goal_new) as meta_linea_nueva'),
                        DB::raw('SUM(sales.goal_migration) as meta_migration'),
                        DB::raw('SUM(sales.goal_porta) as meta_porta')
                    )
                    ->whereBetween('sales.created_at', [$starDate, $endDate])
                    ->where([
                        ['sales.cut', $cut ],
                        ['point_sales.channel', $channel],
                        ['point_sales.regional', $regional]
                    ])
                   ->GROUPBY('point_sales.department') 
        ->get();

        return response()->json(["detalle"=>$detalle]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $starDate = Carbon::now()->format('Y-m-d');
        $endDate = Carbon::now()->copy()->endOfDay();
        $corte = $request->input('cut');
        $idP = $request->input('point_sale_id');
        $option = $request->input('option');
        $new = $request->input('new');
        $migration = $request->input('migration');
        $cover = $request->input('cover_page');
        if($option === 1){
            $repet = DB::table('sales')
            ->select('cut', 'point_sale_id')
            ->whereBetween('sales.created_at', [$starDate, $endDate])
            ->where([
                ['cut',$corte],
                ['point_sale_id',$idP]
            ])
            ->get();
        
            if(count($repet) >= 1){
                return response()->json("repetido");
            }else{
                $sale = Sale::create($request->input());
            }
        }else{
            $app = DB::table('sales')
            ->select('cut','point_sale_id')
                ->whereBetween('sales.created_at', [$starDate, $endDate])
                ->where([
                    ['cut',$corte],
                    ['point_sale_id',$idP]
                ])
                ->update(["new"=>$new,"migration"=>$migration,"cover_page"=>$cover]);
            return response()->json(["punto actualizado"=>$app]);
        }
        

        //$sale = Sale::create($request->input());
        //return response()->json(["point_sale_id"=>$idP, "corte"=>$corte]);
        //return response()->json(["sale"=>$sale]);
    }

    public function getPointsExecution()
    {
        $starDate = Carbon::now()->format('Y-m-d');
        $endDate = Carbon::now()->copy()->endOfDay();
        $points = PointSale::withCount(['sales'=> function($query) use ($starDate, $endDate){ 
            $query->whereBetween('sales.created_at', [$starDate, $endDate]);
        }])
        ->having('sales_count', '=', 0)
        ->orderBy('point_sales.regional')
        ->orderBy('point_sales.channel')
        ->orderBy('point_sales.city')
        ->get();
        return response()->json(["puntos"=>$points]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}
