<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointSale extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'regional',
        'department',
        'city',
        'channel',
        'code',
    ];

    public function sales(){
        return $this->hasMany('App\Models\Sale');
    }
}
