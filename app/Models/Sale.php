<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = [
        'cut',
        'identification_card',
        'new',
        'migration',
        'cover_page',
        'point_sale_id',
        'turno_r',
        'turno_c',
        'turno_total',
        'goal_pos',
        'goal_rgu',
        'rgu',
        'goal_new',
        'goal_migration',
        'goal_porta'
    ];

    public function point_sale(){
        return $this->belongsTo('App\Models\PointSale');
    }
}
