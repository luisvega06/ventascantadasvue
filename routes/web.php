<?php

use Illuminate\Support\Facades\Route;
use App\Exports\SalesExport;
use Maatwebsite\Excel\Facades\Excel;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/sales/create', function (){
    return view('sale.create');
});

Route::get('/report/sales', function (){
    return view('report.reporte');
});

Route::get('/auth/login', function (){
    return view('auth.login');
});

Route::get('/excel', function(){
    return Excel::download(new SalesExport, 'Cantado_Regional.xlsx');
});