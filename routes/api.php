<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('/point_sales',App\Http\Controllers\PointSaleController::class);
Route::apiResource('/sales',App\Http\Controllers\SaleController::class);
Route::get('/deparments/{regional}', [App\Http\Controllers\PointSaleController::class, 'searchDepartment']);
Route::get('/channels/{department}', [App\Http\Controllers\PointSaleController::class, 'searchChannel']);
Route::get('/stores/{channel}/{department}', [App\Http\Controllers\PointSaleController::class, 'searchStore']);
Route::get('/reports/{cut}/{option}', [App\Http\Controllers\SaleController::class, 'reports']);
Route::get('/points/execution', [App\Http\Controllers\SaleController::class, 'getPointsExecution']);

Route::get('/report/department/{regional}/{cut}/{option}', [App\Http\Controllers\SaleController::class, 'reportDepartment']);
Route::get('/points/{department}/{cut}/{option}', [App\Http\Controllers\SaleController::class, 'reportPoint']);
Route::get('/goal/{id}', [App\Http\Controllers\SaleController::class, 'getGoal']);


// nueva reporte en tabla adicional

Route::get('/reports_channel/{cut}/{option}', [App\Http\Controllers\SaleController::class, 'reportsChannel']);
Route::get('/reports_regional_new/{channel}/{cut}/{option}', [App\Http\Controllers\SaleController::class, 'reporRegionalMew']);
Route::get('/reports_r_c_d/{regional}/{channel}/{cut}/{option}', [App\Http\Controllers\SaleController::class, 'RegionalChannelDepartment']);