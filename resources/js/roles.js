const roles = (rol) => {
    const validaciones = {
        'ADMINISTRADOR': '/sales/create',
        'GESTOR': '/sales/create',
        'COORDINADOR': '/sales/create',
        'ANALISTA': '/sales/create',
        'APRENDIZ': '/report/sales',
        'ASISTENTE': '/report/sales',
        'AUXILIAR': '/report/sales',
        'DESARROLLADOR': '/report/sales',
        'DIRECTOR': '/report/sales',
        'DIRECTOR CANAL': '/report/sales',
        'DIRECTOR TERRITORIO': '/report/sales',
        'ENTRENADOR': '/report/sales',
        'ENTRENADOR B2C': '/report/sales',
        'ENTRENADOR CANAL': '/report/sales',
        'ENTRENADOR CANALES': '/report/sales',
        'ENTRENADOR GESTIONA': '/report/sales',
        'ENTRENADOR DIGITAL': '/report/sales',
        'ENTRENADOR DEALER COSTA': '/report/sales',
        'ENTRENADOR LIDER': '/report/sales',
        'ENTRENADOR LIDER B2C': '/report/sales',
        'ENTRENADOR LIDER CANAL': '/report/sales',
        'ENTRENADOR LIDER DIGITAL': '/report/sales',
        'ESPECIALISTA': '/sales/create',
        'ESPECIALISTA ENTRENAMIENTO': '/report/sales',
        'GERENTE': '/report/sales',
        'GERENTE OPERACIONES': '/report/sales',
        'GERENTE SUB TERRITORIO': '/report/sales',
        'LIDER': '/sales/create',
        'PRESIDENTE': '/report/sales',
        'SOPORTE': '/report/sales',
        'SUPERVISOR': '/report/sales',
        'VICEPRESIDENTE': '/report/sales',
        'ADMINISTRADOR TIENDA': '/sales/create',
        'ADMINISTRATIVO': '/sales/create',
        'COORDINADOR TIENDA': '/sales/create',
        'EJECUTIVO': '/sales/create',
        'EJECUTIVO CUENTA': '/sales/create',
        'EJECUTIVO SERVICIO': '/sales/create',
        'SUPERVISOR': '/sales/create'



    }


    return validaciones[rol];

}

export default roles;