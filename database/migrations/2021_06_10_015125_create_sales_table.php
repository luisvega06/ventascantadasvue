<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->integer('cut');
            $table->integer('identification_card');
            $table->integer('new');
            $table->integer('migration');
            $table->integer('cover_page');
            $table->integer('rgu');
            $table->integer('turno_r');
            $table->integer('turno_c');
            $table->integer('turno_total');
            $table->integer('goal_pos');
            $table->integer('goal_rgu');
            $table->integer('goal_new');
            $table->integer('goal_migration');
            $table->integer('goal_porta');

            $table->unsignedBigInteger('point_sale_id');
            $table->foreign('point_sale_id')->references('id')->on('point_sales')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
